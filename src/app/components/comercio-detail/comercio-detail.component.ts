import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Comercio } from '../../models/comercio.model';

import { ComercioService } from '../../services/comercio.service';

@Component({
  selector: 'app-comercio-detail',
  templateUrl: './comercio-detail.component.html',
  styleUrls: ['./comercio-detail.component.css'],
  providers: [ComercioService]
})
export class ComercioDetailComponent implements OnInit {

	public idComercio: string;
	public comercios:Array<Comercio>;
	public comercio: Comercio;

  constructor(
  	private _route: ActivatedRoute,
  	private _comercioService: ComercioService
  ) { }

  ngOnInit() {
  	this.idComercio = this._route.snapshot.paramMap.get("id");
  	this.getComercios();
  }

  getComercioUniq() {
  	this.comercio = this.comercios.find(val => val.properties.id.toString() === this.idComercio);
  	console.log(this.comercio);
  }

  getComercios() {
  	this._comercioService.getComercios().subscribe(
  		response => {
  			this.comercios = response.features;
  			this.getComercioUniq();
  		},
  		error => {
  			console.log('error -> ',error);
  		}
  	);
  }
}
