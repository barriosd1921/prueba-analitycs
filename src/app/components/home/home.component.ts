import { Component, OnInit } from '@angular/core';
import { ComercioService } from '../../services/comercio.service';
import { Comercio } from '../../models/comercio.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ComercioService]
})
export class HomeComponent implements OnInit {

  public comercios:Array<Comercio>;

  constructor(
  	private _comercioService: ComercioService
  ) { }

  ngOnInit(): void {
  	this.getComercios();
  }

  filterComercio(item: Comercio) {
  	return Object.keys(item.properties).length > 0
  }

  getComercios() {
  	this._comercioService.getComercios().subscribe(
  		response => {
  			this.comercios = response.features;
  		},
  		error => {
  			console.log('error -> ',error);
  		}
  	);
  }
}
