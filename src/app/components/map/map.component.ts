import { Component, OnInit, Input } from '@angular/core';
import { MapService } from '../../services/map.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  
  @Input() coordinates: Array<number>;

  constructor(private map: MapService) { }

  ngOnInit() {
    this.map.buildMap(this.coordinates);
  }

}