import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { ComercioDetailComponent } from './components//comercio-detail/comercio-detail.component';

const routes: Routes = [
	{path: '', component: HomeComponent},
	{path: 'comercio-detail/:id', component: ComercioDetailComponent},
	{path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
