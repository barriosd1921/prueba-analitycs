import { Properties } from './propiedades.model';
import { Geometry } from './geometry.model';

export class Comercio {
	constructor(
		public geometry: Geometry,
		public properties: Properties
	){}
}