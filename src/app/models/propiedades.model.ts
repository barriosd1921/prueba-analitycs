export class Properties {
	constructor(
		public name: string,
		public address: string,
		public days: string,
		public id: number,
		public nit: string,
		public owner: string,
		public phone: string,
		public sales: string,
		public schedule: string,
		public type: string
	){}
}