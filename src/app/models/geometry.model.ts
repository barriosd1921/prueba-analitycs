export class Geometry {
	constructor(
		public coordinates: Array<string>,
		public type: string
	){}
}