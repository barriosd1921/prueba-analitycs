import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

import * as mapboxgl from 'mapbox-gl';

@Injectable({
  providedIn: 'root'
})
export class MapService {

  public mapbox = (mapboxgl as typeof mapboxgl);
  public map: mapboxgl.Map;
  public style = `mapbox://styles/mapbox/streets-v11`;
  public zoom = 15;

  constructor() {
    this.mapbox.accessToken = environment.mapBoxToken;
  }

  buildMap(coordinates) {
    this.map = new mapboxgl.Map({
      container: 'map',
      style: this.style,
      zoom: this.zoom,
      center: coordinates
    });
    this.map.addControl(new mapboxgl.NavigationControl());
    }
}