import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders }from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ComercioService {
  
  public url_base:string;

  constructor(
  	public _http: HttpClient
  ) {
  	this.url_base = "https://alw-lab.herokuapp.com/"; 
  }

  getComercios(): Observable<any>{
  	return this._http.get(this.url_base+'commerces/layer');
  }
}
